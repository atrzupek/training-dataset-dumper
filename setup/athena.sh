# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
# If you need to run on a nightly, see ATLINFR-4697
asetup Athena,23.0.20

# add h5ls
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
if [[ $SCRIPT_PATH =~ / ]] ; then
    source ${SCRIPT_PATH%/*}/add-h5-tools.sh
else
    source setup/add-h5-tools.sh
fi

# make the job fail on flake8 warnings.
export FLAKE8_ATLAS_ERROR=1
