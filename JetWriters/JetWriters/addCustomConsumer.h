#ifndef ADD_CUSTOM_CONSUMER_H
#define ADD_CUSTOM_CONSUMER_H

#include "JetWriters/JetElement.h"

#include "xAODTracking/TrackParticleFwd.h"
#include "xAODBase/IParticle.h"
#include "HDF5Utils/Writer.h"

#include <optional>
#include <type_traits>

namespace jetwriters::detail {

  using H5Utils::Compression;
  template <typename T>
  using JetConsumers = H5Utils::Consumers<const JetElement<T>&>;
  template <typename F, typename S = xAOD::Jet>
  using JetPair = std::pair<const F*, const S*>;

  // type checking utilities
  template <typename... Ts>
  using is_all_iparticle = typename std::conjunction<
    std::is_base_of<xAOD::IParticle, Ts>...>;
  template <typename T>
  using getbase = typename std::remove_const<
    typename std::remove_pointer<T>::type>;


  // check edm_name map for remaining variable name replacement
  //
  class EdmNameMap
  {
  public:
    EdmNameMap(const std::map<std::string, std::string>&);
    std::string operator()(const std::string&);
    void checkRemaining();
  private:
    std::map<std::string,std::string> m_remap;
    std::set<std::string> m_used;
  };


  // function to build composite accessor function
  template <typename I, typename O, typename Element, typename Base>
  auto getComposite(Base base, Element acc, O def) {
    return [acc, base, def](const I& in) {
        const auto& base_pair = base(in);
        if (!base_pair.first || !base_pair.second) return def;
        return acc(base_pair);
    };
  }

  // custom getters
  //
  // if nothing matches we return nullopt
  template <typename F, typename S = xAOD::Jet>
  std::optional<std::function<float(const JetPair<F,S>&)>> getFloat(
    const std::string&,
    EdmNameMap&)
  {
    return std::nullopt;
  }

  template <typename T, typename U>
  using JetGetter = std::optional<std::function<U(const JetPair<T>&)>>;
  //
  // the concrete instances of custom getters start here
  template <>
  JetGetter<xAOD::TrackParticle, float> getFloat<xAOD::TrackParticle>(
    const std::string& name,
    EdmNameMap& edm_name);
  //


  // general IParticle filler
  template <typename T, typename U>
  std::optional<std::function<float(const JetPair<T,U>&)>> getIParticleFloat(
    const std::string& name)
  {
    using Arg = JetPair<T,U>;
    if (name == "pt") {
      return [](const Arg& a) -> float { return a.first->pt(); };
    } else if (name == "eta") {
      return [](const Arg& a) -> float { return a.first->eta(); };
    } else if (name == "phi") {
      return [](const Arg& a) -> float { return a.first->phi(); };
    } else if (name == "mass") {
      return [](const Arg& a) -> float { return a.first->m(); };
    } else if (name == "energy") {
      return [](const Arg& a) -> float { return a.first->e(); };
    } else if (name == "deta") {
      return [](const Arg& a) -> float {
        return a.first->eta() - a.second->eta();
      };
    } else if (name == "dphi") {
      return [](const Arg& a) -> float {
        return a.first->p4().DeltaPhi(a.second->p4());
      };
    } else if (name == "dr") {
      return [](const Arg& a) -> float {
        return a.first->p4().DeltaR(a.second->p4());
      };
    } else if (name == "abs_eta") {
      return [](const Arg& a) -> float {
        return std::abs(a.first->eta());
      };
    } else if (name == "ptfrac") {
      return [](const Arg& a) -> float {
        return a.first->pt() / a.second->pt();
      };
    } else if (name == "ptrel") {
      return [](const Arg& a) -> float {
        return a.first->p4().Vect().Perp(a.second->p4().Vect());
      };
    }
    return std::nullopt;
  }


  ////////////////////////////////////////
  // top level filler for custom consumers.
  ////////////////////////////////////////
  template <typename T, typename B, typename C = H5Utils::Consumers<T>>
  void addCustomConsumer(C& cons,
                         B base,
                         const std::string& name,
                         Compression cmp,
                         EdmNameMap& edmName) {

    using I = typename C::input_type;
    using BO = decltype(base(std::declval<I>()));
    using F = typename getbase<typename BO::first_type>::type;
    using S = typename getbase<typename BO::second_type>::type;

    // A lot of these might be IParticle-based
    if constexpr (is_all_iparticle<F,S>::value) {
      if (auto acc = getIParticleFloat<F,S>(edmName(name))) {
        cons.add(name, getComposite<I>(base, *acc, NAN), NAN, cmp);
        return;
      }
    }

    // if we can't find an IParticle consumer, use "custom" ones
    if (auto acc = getFloat<F,S>(edmName(name), edmName)) {
      cons.add(name, getComposite<I>(base, *acc, NAN), NAN, cmp);
      return;
    }

    throw std::runtime_error("no match for custom consumer '" + name + "'");
  }


}

#endif
