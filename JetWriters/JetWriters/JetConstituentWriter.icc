// this is -*- C++ -*-
// implementation for JetConstituentWriter

#include "addCustomConsumer.h"
#include "JetWriters/JetElement.h"

namespace jetwriters::detail {

  using OutputBlock = JetConstituentWriterConfig::OutputBlock;

  template <typename O, typename C, typename B>
  void addConsumer(C& consumers,
                   B base,
                   const std::string& name,
                   O def,
                   H5Utils::Compression c,
                   EdmNameMap& edmName)
  {
    using Acc = SG::AuxElement::ConstAccessor<O>;
    using I = typename C::input_type;
    std::string edm_name = edmName(name);
    consumers.add(
      name,
      [a=Acc(edm_name),b=base,d=def](const I& in) -> O {
        if (!b(in).first) return d;
        return a(*b(in).first);
      },
      def,
      c);
  }

  template <typename T, typename B>
  void buildOutputBlock(H5Utils::Consumers<const JetElement<T>&>& c,
                        const OutputBlock& cfg,
                        const B base)
  {
    using CO = JetConstituentWriterConfig::Output;
    using CT = JetConstituentWriterConfig::Type;
    auto f = H5Utils::Compression::STANDARD;
    auto h = H5Utils::Compression::HALF_PRECISION;
    EdmNameMap m(cfg.edm_name);

    for (const auto& o: cfg.outputs) {
      std::string n = o.name;
      const B b = base;
      switch (o.type) {
      case CT::CUSTOM: addCustomConsumer<T>(c, b, n, f, m); break;
      case CT::CUSTOM_HALF: addCustomConsumer<T>(c, b, n, h, m); break;
      case CT::UCHAR: addConsumer<unsigned char>(c, b, n, 0, f, m); break;
      case CT::CHAR: addConsumer<char>(c, b, n, -1, f, m); break;
      case CT::INT: addConsumer<int>(c, b, n, -1, f, m); break;
      case CT::HALF: addConsumer<float>(c, b, n, NAN, h, m); break;
      case CT::FLOAT: addConsumer<float>(c, b, n, NAN, f, m); break;
      default: break;
      }
    }
    if (!cfg.allow_unused_edm_names) m.checkRemaining();
  }

  template <size_t pos = 0, typename T, typename A>
  std::set<std::string> buildAssociations(
    H5Utils::Consumers<const JetElement<T>&>& c,
    const A& assocs,
    const std::map<std::string, OutputBlock>& cfgs)
  {
    std::set<std::string> used;
    if constexpr (pos != std::tuple_size<A>::value) {
      const auto& assoc = std::get<pos>(assocs);
      using E = typename std::tuple_element<pos, A>::type;
      using R = decltype(E::accessor(std::declval<const JetElement<T>&>()));
      using O = std::pair<R, const xAOD::Jet*>;
      if (cfgs.count(assoc.name)) {
        auto base = [b=assoc.accessor](const auto& in) -> O {
          return {b(in), in.jet};
        };
        buildOutputBlock(c, cfgs.at(assoc.name), base);
        used.insert(assoc.name);
      }
      used.merge(buildAssociations<pos+1>(c, assocs, cfgs));
    }
    return used;
  }

  template <typename T, typename A>
  auto buildConsumers(const JetConstituentWriterConfig& cfg,
                      A assocs) {
    H5Utils::Consumers<const JetElement<T>&> cons;
    buildOutputBlock(
      cons,
      cfg.constituent,
      [](const auto& in) -> std::pair<const T*, const xAOD::Jet*> {
        return {in.constituent, in.jet};
      });
    auto used = buildAssociations(cons, assocs, cfg.associated);

    // make sure all associations are used
    if (used.size() != cfg.associated.size()) {
      std::string err = "undefined associators:";
      for (const auto& ass: cfg.associated) {
        if (!used.count(ass.first)) err += (" " + ass.first);
        throw std::runtime_error(err);
      }
    }

    cons.add("valid", [](const auto&) { return true; }, false);
    return cons;
  }

}

template <typename T, typename A>
JetConstituentWriter<T,A>::JetConstituentWriter(
  H5::Group& group,
  const JetConstituentWriterConfig& config,
  A assocs):
  m_writer(
    group,
    config.name,
    jetwriters::detail::buildConsumers<T>(config, assocs),
    {{config.size}})
{
  if (config.name.size() == 0) {
    throw std::logic_error("output name not specified");
  }
}

template <typename T, typename A>
JetConstituentWriter<T,A>::JetConstituentWriter(JetConstituentWriter&&) = default;

template <typename T, typename A>
void JetConstituentWriter<T,A>::write(const xAOD::Jet& j,
                                      const std::vector<const T*>& cons)
{
  std::vector<ElementType> outputs;
  for (const auto* constituent: cons) {
    outputs.emplace_back(ElementType(constituent, &j));
  }
  m_writer.fill(outputs);
}

template <typename T, typename A>
void JetConstituentWriter<T, A>::flush()
{
  m_writer.flush();
}
