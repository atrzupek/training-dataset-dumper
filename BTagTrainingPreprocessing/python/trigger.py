from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

# needed for trigger decision tool
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg

# tools!
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg, JetParticleFixedConeAssociationAlgCfg)
from ParticleJetTools.ParticleJetToolsConfig import (
    getJetDeltaRFlavorLabelTool)


##############################################################################
###################### internal functions ####################################
##############################################################################

def getLabelingBuilderAlg(cfgFlags):
    label_tools = []
    for ptype in ['BHadrons','CHadrons','Taus']:
        tool = CompFactory.CopyFlavorLabelTruthParticles(f'{ptype}Builder')
        tool.ParticleType = f'{ptype}Final'
        tool.OutputName = f'TruthLabel{ptype}Final'
        tool.OutputLevel = cfgFlags.Exec.OutputLevel
        label_tools.append(tool)
    labelCollectionBuilder = CompFactory.JetAlgorithm("LabelBuilderAlg")
    labelCollectionBuilder.Tools = label_tools
    labelCollectionBuilder.OutputLevel = cfgFlags.Exec.OutputLevel
    return labelCollectionBuilder


def triggerJetGetterCfg(cfgFlags, chain, temp_jets, temp_btag='',
                        additional_chains=[]):
    ca = ComponentAccumulator()

    # This is (was?) needed for TDT
    ca.merge(MetaDataSvcCfg(cfgFlags))
    tdtca = TrigDecisionToolCfg(cfgFlags)
    ca.merge(tdtca)

    # Local component to move trigger elements into collections so we
    # can access them inside offline code
    jetGetter = CompFactory.TriggerJetGetterAlg(
        f'TriggerJetGetter_{chain}')
    jetGetter.triggerDecisionTool = tdtca.getPrimary()
    jetGetter.bJetChain = chain
    jetGetter.additionalChains = additional_chains
    jetGetter.outputJets = temp_jets
    jetGetter.outputBTag = temp_btag
    jetGetter.jetModifiers = [getJetDeltaRFlavorLabelTool()]
    jetGetter.TrigJetCollectionKey = "HLT_.*"
    ca.addEventAlgo(jetGetter)

    return ca


def offlineBTagMatcherCfg(
        flags,
        btag_key,
        jet_key,
        taggers_to_copy=['DL1r','DL1dv00'],
        truth_labels=[
            'HadronConeExclTruthLabelID',
            'HadronConeExclExtendedTruthLabenlID',
        ],
        source_btag_key='BTagging_AntiKt4EMPFlow',
        source_jet_key='AntiKt4EMPFlowJets'):

    ca = ComponentAccumulator()
        # match to offline jets, pull out some info
    matcher = CompFactory.TriggerBTagMatcherAlg('matcher')
    matcher.offlineBtagKey = source_btag_key
    matcher.triggerBtagKey = btag_key
    matcher.floatsToCopy = {
        f'{tagger}_p{flav}':f'OfflineMatched{tagger}_p{flav}' for tagger in taggers_to_copy for flav in 'bcu'
    }
    matcher.offlineJetKey = source_jet_key
    matcher.triggerJetKey = jet_key
    truth_labels = [
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
    ]
    matcher.jetIntsToCopy = {
        x:f'OfflineMatched{x}' for x in truth_labels
    }
    ca.addEventAlgo(matcher)
    return ca


def jetMatcherCfg(
        flags,
        target_jets,
        source_jets,
        fast_dips=[],
        float_map={},
        iparticles=[],
        dR='deltaRToMatchedJet',
    ):
    ca = ComponentAccumulator()
    fd_vars = {f'{t}_p{x}':f'{t}_p{x}' for x in 'cub' for t in fast_dips}
    iparticlesToCopy={x:x for x in iparticles}
    matcher = CompFactory.JetMatcherAlg(
        name=f'Matcher_{target_jets}_from_{source_jets}',
        targetJet=target_jets,
        sourceJet=source_jets,
        floatsToCopy=(fd_vars | float_map),
        iparticlesToCopy=iparticlesToCopy,
        dR=dR
    )
    ca.addEventAlgo(matcher)
    return ca


##############################################################################
###################### top level functions ###################################
##############################################################################

def pflowDumper(cfgFlags, chain, additional_chains=[]):

    # this thing only exists in full athena, thus the import here
    from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg

    ca = ComponentAccumulator()

    # build labeling collections
    labelAlg = getLabelingBuilderAlg(cfgFlags)
    ca.addEventAlgo(labelAlg)

    # get jets
    temp_btag, temp_jets = 'tempBtag', 'tempJets'
    ca.merge(triggerJetGetterCfg(
        cfgFlags,
        chain=chain,
        additional_chains=additional_chains,
        temp_jets=temp_jets,
        temp_btag=temp_btag,
    ))

    # match to offline jets, pull out some info
    ca.merge(offlineBTagMatcherCfg(
        cfgFlags,
        btag_key=temp_btag,
        jet_key=temp_jets))
    # match to associated pflow jets
    ca.merge(jetMatcherCfg(
        cfgFlags, temp_jets,
        source_jets='HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf',
        fast_dips=['fastDIPS20211215','dips20211116'],
        iparticles=[],
        dR='deltaRToPFlowJet'
    ))
    # match to associated EMTopo jets
    ca.merge(jetMatcherCfg(
        cfgFlags, temp_jets,
        source_jets='HLT_AntiKt4EMTopoJets_subjesIS_fastftag',
        fast_dips=['fastDips'],
        iparticles=[],
        dR='deltaRToEMTopoJet'
    ))

    # associate fullscan tracks to the jets
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca.merge(BTagTrackAugmenterAlgCfg(
            cfgFlags,
            TrackCollection=fs_tracks,
            PrimaryVertexCollectionName=fs_vertices))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        name='_'.join(['PoorMansIpAugmenter',fs_tracks, fs_vertices]),
        trackContainer=fs_tracks,
        primaryVertexContainer=fs_vertices))
    ca.merge(JetParticleAssociationAlgCfg(
        cfgFlags,
        JetCollection=temp_jets,
        InputParticleCollection=fs_tracks,
        OutputParticleDecoration='FsTracks'))
    ca.merge(JetParticleFixedConeAssociationAlgCfg(
        cfgFlags,
        fixedConeRadius=0.5,
        JetCollection=temp_jets,
        InputParticleCollection=fs_tracks,
        OutputParticleDecoration='FsTracksFixedCone'))

    return ca


def emtopoDumper(cfgFlags, chain, additional_chains=[]):
    ca = ComponentAccumulator()

    # build labeling collections
    labelAlg = getLabelingBuilderAlg(cfgFlags)
    ca.addEventAlgo(labelAlg)

    # get jets
    temp_jets = 'tempEmtopoJets'
    ca.merge(triggerJetGetterCfg(
        cfgFlags,
        chain=chain,
        additional_chains=additional_chains,
        temp_jets=temp_jets
    ))

    # associate fullscan tracks to the jets
    tracks = 'HLT_IDTrack_JetSuper_FTF'
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    for tr in [tracks, fs_tracks]:
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                name=f'PoormanIp_{tr}',
                trackContainer=tr
            ))

    ca.merge(JetParticleAssociationAlgCfg(
        cfgFlags,
        JetCollection=temp_jets,
        InputParticleCollection=fs_tracks,
        OutputParticleDecoration='FsTracks'))
    ca.merge(JetParticleAssociationAlgCfg(
        cfgFlags,
        JetCollection=temp_jets,
        InputParticleCollection=tracks,
        OutputParticleDecoration='superTracks'))
    ca.merge(JetParticleFixedConeAssociationAlgCfg(
        cfgFlags,
        fixedConeRadius=0.5,
        JetCollection=temp_jets,
        InputParticleCollection=fs_tracks,
        OutputParticleDecoration='superTracksFixedCone'))


    return ca
