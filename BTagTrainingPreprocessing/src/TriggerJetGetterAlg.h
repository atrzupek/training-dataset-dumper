/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef DERIVATION_FRAMEWORK_FLAVOUR_TAG_BTAG_TRIGGER_ELEMENT_TESTER_H
#define DERIVATION_FRAMEWORK_FLAVOUR_TAG_BTAG_TRIGGER_ELEMENT_TESTER_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTaggingContainer.h"
#include "JetInterface/IJetModifier.h"
#include "xAODEventInfo/EventInfo.h"

namespace Trig {
  class TrigDecisionTool;
}

class TriggerJetGetterAlg: public EL::AnaAlgorithm
{
public:
  TriggerJetGetterAlg(const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  PublicToolHandle<Trig::TrigDecisionTool> m_triggerDecision {
    this, "triggerDecisionTool", "",
      "Input TrigDecision Tool" };
  ToolHandleArray<IJetModifier> m_jetModifiers {
    this, "jetModifiers", {},
      "List of Jet modifiers" };
  Gaudi::Property<std::string> m_bjetChain {
    this, "bJetChain", "HLT_.*", "Regex for chains to match"};
  Gaudi::Property<std::vector<std::string>> m_additionalChains {
    this, "additionalChains", {}, "Additional required chains"
  };
  SG::ReadHandleKey< xAOD::JetContainer > m_trigJetKey  {
    this, "TrigJetCollectionKey", "HLT_bJets",
    "Input Jet collection name" };
  SG::WriteHandleKey<xAOD::JetContainer> m_outputJets {
    this, "outputJets", "SelectedJets", "Output jet container name"};
  SG::WriteHandleKey<xAOD::BTaggingContainer> m_outputBTag {
    this, "outputBTag", "SelectedBTag", "Output btag container name"};

  // links to / from the b-tagging object
  SG::AuxElement::Accessor<ElementLink<xAOD::JetContainer>> m_jetLink;
  SG::AuxElement::Accessor<ElementLink<xAOD::BTaggingContainer>> m_btagLink;

};

#endif
