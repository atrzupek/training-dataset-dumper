#ifndef VARIABLE_MULE_HH
#define VARIABLE_MULE_HH

#include "StoreGate/WriteDecorHandleKeyArray.h"
#include "StoreGate/ReadDecorHandleKeyArray.h"
#include "Gaudi/Algorithm.h"
#include "AthenaBaseComps/AthCheckMacros.h"

#include <type_traits>

template <typename T>
struct MatchedPair
{
  typename T::const_value_type from;
  typename T::const_value_type to;
};

template <typename T, typename C>
class VariableMule
{
public:
  std::map<std::string,std::string> toCopy;
private:
  using RC = SG::ReadDecorHandleKeyArray<C>;
  using WC = SG::WriteDecorHandleKeyArray<C>;
  RC m_fromKeys;
  WC m_toKeys;
  T m_default;
public:
  VariableMule(T default_value): m_default(default_value) {}
  StatusCode initialize(Gaudi::Algorithm* parent,
                        const std::string& from,
                        const std::string& to) {
    for (const auto& key: toCopy) {
      std::string fullfrom = from + "." + key.first;
      std::string fullto = to + "." + key.second;
      std::string doc = "Key to move " + key.first + "->" + key.second;
      m_fromKeys.emplace_back(parent, "read_" + key.first, fullfrom, doc);
      m_toKeys.emplace_back(parent, "write_" + key.second, fullto, doc);
    }
    ATH_CHECK(m_fromKeys.initialize());
    ATH_CHECK(m_toKeys.initialize());
    return StatusCode::SUCCESS;
  }
  void copy(const std::vector<MatchedPair<C>>& pairs, const EventContext& cxt)
    const {
    std::vector<SG::ReadDecorHandle<C,T>> from;
    std::vector<SG::WriteDecorHandle<C,T>> to;
    size_t n_keys = m_fromKeys.size();
    for (size_t iii = 0; iii < n_keys; iii++) {
      const auto& fromKey = m_fromKeys.at(iii);
      const auto& toKey = m_toKeys.at(iii);
      from.emplace_back(fromKey, cxt);
      to.emplace_back(toKey, cxt);
    }
    for (const auto& pair: pairs) {
      for (size_t iii = 0; iii < n_keys; iii++) {
        if (pair.from) {
          to.at(iii)(*pair.to) = from.at(iii)(*pair.from);
        } else {
          to.at(iii)(*pair.to) = m_default;
        }
      }
    }
  }
};


#endif
