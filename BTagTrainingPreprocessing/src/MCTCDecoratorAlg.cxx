#include "MCTCDecoratorAlg.h"

#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"

#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"


MCTCDecoratorAlg::MCTCDecoratorAlg(
  const std::string& name, ISvcLocator* loc )
  : AthReentrantAlgorithm(name, loc) {}

StatusCode MCTCDecoratorAlg::initialize() {
  ATH_MSG_DEBUG( "Inizializing " << name() << "... " );

  ATH_CHECK( m_classifier.retrieve() );
  ATH_CHECK( m_TruthContainerKey.initialize() );
  m_dec_type = m_TruthContainerKey.key() + "." + m_dec_type.key();
  m_dec_origin = m_TruthContainerKey.key() + "." + m_dec_origin.key();
  ATH_CHECK( m_dec_type.initialize() );
  ATH_CHECK( m_dec_origin.initialize() );
  
  return StatusCode::SUCCESS;
}

StatusCode MCTCDecoratorAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG( "Executing " << name() << "... " );

  // retrieve truth collection
  SG::ReadHandle<xAOD::TruthParticleContainer> truths(m_TruthContainerKey, ctx);
  ATH_CHECK( truths.isValid() );
  
  // set up decorators
  using TPC = xAOD::TruthParticleContainer;
  SG::WriteDecorHandle<TPC, unsigned int> dec_type(m_dec_type, ctx);
  SG::WriteDecorHandle<TPC, unsigned int> dec_origin(m_dec_origin, ctx);

  // decorate particles
  for (const auto part: *truths) {
    auto [type, origin] = m_classifier->particleTruthClassifier(part);
    dec_type(*part) = type;
    dec_origin(*part) = origin;
  }

  return StatusCode::SUCCESS;
}
