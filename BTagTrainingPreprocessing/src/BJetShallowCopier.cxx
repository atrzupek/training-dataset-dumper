#include "BJetShallowCopier.hh"
#include "SafeShallowCopy.hh"

#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTaggingContainer.h"

// we have some really awesome workarounds for AnalysisBase, which
// doesn't support transient ElementLink.
//
// We need to store the event in TStore to make the links work.
// TStore wants ownership, which isn't needed in the Gaudi releases.
// Fortunately, there's a protected function that allows us to disable
// the ownership.
//
// So to get access to this protected function, we we create a derived
// class which wraps TStore. Then retrieve TActiveStore and static
// cast it to our special class, which allows us to use TStore and
// element links.
//
#ifdef XAOD_STANDALONE
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TStore.h"

class BorrowingTStore: public xAOD::TStore
{
public:
  template< typename T >
  void recordBorrowed(const std::unique_ptr<T>& obj,
                      const std::string& key ) {
    // this is just the first part of the unique_ptr function
    //
    // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Control/xAODRootAccess/xAODRootAccess/TStore.icc#0095
    //
    // we don't really care about cases with no dictionary for now
    //
    auto type_name = SG::normalizedTypeinfoName(typeid(T));
    auto isOwner = kFALSE;       // don't take ownership
    auto isConst = kFALSE;        // don't pass as const
    auto sc = record(obj.get(), key, type_name, isOwner, isConst);
    if (!sc.isSuccess()) throw std::runtime_error("bad return code");
  }
};

// this is a bit of a hack to make sure we never store containers with
// the same name. Yay for using global variables to hide global variables.
static std::atomic<size_t> tstore_counter(0);

#endif // XAOD_STANDALONE

namespace {
  template <typename T>
  using Acc_t = SG::AuxElement::ConstAccessor<T>;
  template <typename T>
  using Dec_t = SG::AuxElement::Decorator<T>;
  using JetLink_t = ElementLink<xAOD::JetContainer>;
  using BTagLink_t = ElementLink<xAOD::BTaggingContainer>;

}

static const std::string JET_LINK_NAME = "jetLink";
struct ShallowCopyLinkers
{
  Acc_t<JetLink_t> jetAcc;
  Dec_t<JetLink_t> jetDec;
  Acc_t<BTagLink_t> btagAcc;
  Dec_t<BTagLink_t> btagDec;
  ShallowCopyLinkers(const std::string& btag_link):
    jetAcc(JET_LINK_NAME),
    jetDec(JET_LINK_NAME),
    btagAcc(btag_link),
    btagDec(btag_link)
    {}
};

BJetShallowCopier::BJetShallowCopier(const std::string& btag_link):
  m_links(new ShallowCopyLinkers(btag_link))
{
  if(btag_link.empty()) m_copyBtagObject = false;
}

BJetShallowCopier::~BJetShallowCopier() = default;

BJetShallowCopier::BJetCopy BJetShallowCopier::shallowCopyBJets(
  const xAOD::JetContainer& original_jets) const {

  auto [jets, jetAux] = safeShallowCopyContainer(original_jets);

  // check if we want only a copy of the jets
  if (!m_copyBtagObject) {
    BJetShallowAux aux;
    aux.jetAux = std::move(jetAux);
    return std::make_pair(std::move(jets), std::move(aux));
  }

  // assuming we have at least one b-jet with a valid link, we can get
  // the b-tagging container
  const xAOD::BTaggingContainer* original_btags = nullptr;
  for (const auto& jet: *jets) {
    const auto& btag_link = m_links->btagAcc(*jet);
    if (btag_link.isValid()) {
      const auto* btags = btag_link.getStorableObjectPointer();
      if (original_btags) {
        if (original_btags != btags) {
          throw std::logic_error("found inconsistent btagging links");
        }
      } else {
        original_btags = btags;
      }
    }
  }
  // if we weren't able to find a valid b-tagging link, just return
  // the jets (this might just mean the jet container was empty)
  if (!original_btags) {
    BJetShallowAux aux;
    aux.jetAux = std::move(jetAux);
    return std::make_pair(std::move(jets), std::move(aux));
  }
  // shallow copy the b-tagging container
  auto [btags, btagAux] = safeShallowCopyContainer(*original_btags);

  // Now we have to update the links. Each b-tagging object should
  // point to a jet, so we need to:
  //
  // - loop over the new b-tagging container
  //
  // - find the index that the `jetLink` points to
  //
  // - overwrite the `jetLink` to point to the new jet container, with the
  //   same index.
  //
  // - Retrieve the jet at this jet index, overwrite the
  //   `btaggingLink` to point to the new b-tagging container and the
  //   index of the current b-tagging object
  //
  // This has to be done in two steps because writing new element
  // links invalidates all the old links.
  std::map<std::size_t,std::size_t> jet_index_from_btag_index;
  for (const auto& btag: *btags) {
    JetLink_t old_jet_link = m_links->jetAcc(*btag);
    std::size_t jet_index = (*old_jet_link)->index();
    jet_index_from_btag_index[btag->index()] = jet_index;
  }

  // this is a janky workaround for AnalysisBase. We don't have
  // transient element links, so we store them in our hacked TStore
  //
  // NOTE: THIS WILL LEAK MEMORY IF YOU DON'T CLEAR THE TSTORE!!!
  //
  // There is no way to automatically manage objects that go into this
  // global store!
  //
#ifdef XAOD_STANDALONE
  // Make sure each set we store has a unique name. This might wrap
  // around before the end of time, but hopefully we'll have flushed
  // the old entries by then.
  std::string instance_number = std::to_string(tstore_counter++);
  // now we grab the store and store things
  auto* store = static_cast<BorrowingTStore*>(xAOD::TActiveStore::store());
  store->recordBorrowed(btags, "btags" + instance_number);
  store->recordBorrowed(jets, "jets" + instance_number);
#endif

  for (const auto& btag: *btags) {
    std::size_t jet_index = jet_index_from_btag_index.at(btag->index());
    JetLink_t new_jet_link(*jets, jet_index);
    m_links->jetDec(*btag) = new_jet_link;
    const xAOD::Jet* new_jet = jets->at(jet_index);
    BTagLink_t new_btag_link(*btags, btag->index());
    m_links->btagDec(*new_jet) = new_btag_link;
  }
  BJetShallowAux aux;
  aux.jetAux = std::move(jetAux);
  aux.btag = std::move(btags);
  aux.btagAux = std::move(btagAux);
  return std::make_pair(std::move(jets), std::move(aux));
}
