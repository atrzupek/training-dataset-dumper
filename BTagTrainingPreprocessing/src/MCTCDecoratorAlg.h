#ifndef MCTC_DECORATOR_ALG_HH
#define MCTC_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"

/*
   This is a copy of 
   DerivationFrameworkMCTruth/​TruthClassificationDecorator.h
   to avoid requiring full Athena to access the 
   DerivationFrameworkMCTruth package. 
   
   A cleaner workaround would be to move the 
   ​TruthClassificationDecorator class to the 
   MCTruthClassifier package.
*/

class MCTCDecoratorAlg :  public AthReentrantAlgorithm { 
public:
  
  /** Constructors */
  MCTCDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);
  
  /** Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&)  const override;

private:
  
  ToolHandle<IMCTruthClassifier> m_classifier {
    this, "MCTruthClassifier", "MCTruthClassifier", 
      "MC truth particle classification tool"};
        
  SG::ReadHandleKey< xAOD::TruthParticleContainer > m_TruthContainerKey {
    this, "truthContainer", "TruthParticles",
      "Key for the input truth collection"};

  SG::WriteDecorHandleKey< xAOD::TruthParticleContainer > m_dec_type {
    this, "classifierParticleType", "classifierParticleType", "MCTC particle type"};
  SG::WriteDecorHandleKey< xAOD::TruthParticleContainer > m_dec_origin {
    this, "classifierParticleOrigin", "classifierParticleOrigin", "MCTC particle origin"};
};

#endif