#include "BTagJetWriter.hh"
#include "BTagJetWriterConfig.hh"

#include "BTagJetWriterUtils.hh"
#include "SubstructureAccessors.hh"

#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

BTagJetWriter::BTagJetWriter(
  H5::Group& output_file,
  const BTagJetWriterConfig& config)
{
  using H5Utils::Compression;

  Compression f = Compression::STANDARD;
  Compression h = config.force_full_precision ?
    Compression::STANDARD : Compression::HALF_PRECISION;

  // create the variable fillers
  JetConsumers fillers;

  add_jet_variables(fillers, config, h);

  // now create the writer
  if (config.n_jets_per_event > 0) {
    m_hdf5_jet_writer.reset(
      new JetOutputWriter1D(
        output_file, config.name, fillers, config.n_jets_per_event));
    // we create another writer for the event info if we're writing
    // jets in a per-event way.
    JetConsumers event_fillers;
    add_event_info(event_fillers, config.event_info, f);
    add_event_info(event_fillers, config.event_compressed, h);
    m_event_writer.reset(
      new H5Utils::Writer<0,const JetOutputs&>(
        output_file, config.name + "_event", event_fillers));
  } else {
    add_event_info(fillers, config.event_info, f);
    add_event_info(fillers, config.event_compressed, h);
    m_hdf5_jet_writer.reset(
      new JetOutputWriter(output_file, config.name, fillers));
  }
}

BTagJetWriter::~BTagJetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
}
BTagJetWriter::BTagJetWriter(BTagJetWriter&&) = default;


void BTagJetWriter::write(const std::vector<const xAOD::Jet*> jets,
                          const xAOD::EventInfo* mc_event_info) {
  std::vector<JetOutputs> jos;
  for (const auto* jet: jets) {
    JetOutputs jo;
    jo.event_info = mc_event_info;
    jo.jet = jet;
    jos.push_back(jo);
  }
  m_hdf5_jet_writer->fill(jos);
  if (m_event_writer) {
    JetOutputs jo;
    jo.event_info = mc_event_info;
    m_event_writer->fill(jo);
  }
}
