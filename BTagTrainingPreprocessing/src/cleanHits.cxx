/*
Modules in the silicon tracker are overlapping in phi and z.
If a track passes through one of these overlap regions it will leave
two hits behind in that overlap region where normally it would have only left one hit.
The cleanHits function removes hits which are close in dPhi and dZ if they
come from different detectorElementID. The isGoodHit function removes fake hits coming
from ganged pixels and other problematic hits, reproducing as closely as possible
what is done in the PixelConditionSummaryTool
*/

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "cleanHits.hh"
#include "TVector3.h"
#include <limits>

template <typename T>
using Acc = SG::AuxElement::ConstAccessor<T>;

// Accessors
Acc<int> layer("layer");
Acc<int> bec("bec");
Acc<char> isFake("isFake");
Acc<int> DCSState("DCSState");
Acc<char> broken("broken");
Acc<int> hasBSError("hasBSError");
Acc<unsigned long> deid("detectorElementID");
Acc<char> isSCT("isSCT");

bool isGoodHit(const xAOD::TrackMeasurementValidation* hit) {
  if (isFake(*hit)) return false;
  if (hasBSError(*hit)) return false;
  if (DCSState(*hit)) return false;
  if (broken(*hit)) return false;
  return true;
}

// Function that performs removal of fake and overlapping hits
std::vector<const xAOD::TrackMeasurementValidation*> cleanHits(const xAOD::TrackMeasurementValidationContainer* inHits,
                     bool removeBadHits) {

  std::vector<const xAOD::TrackMeasurementValidation*> outHitsCollection;

  for (const auto *inhit : *inHits) {
    // Remove problematic hits

    if (!isSCT(*inhit) && removeBadHits && !isGoodHit(inhit)) continue;
    // Get input hit position and layer
    TVector3 inHitPos(inhit->globalX(), inhit->globalY(), inhit->globalZ());
    int layerIn = layer(*inhit);

    // Is the hit in the barrel?
    bool isBarrel = bec(*inhit) == 0;

    // Loop over output hits and check if any hit overlaps with hit to be added
    bool overlap=false;
    for (const auto &outhit : outHitsCollection) {
      // If the hits are not on the same layer, nothing to do
      int layerOut = layer(*outhit);
      if (layerIn != layerOut) continue;

      // Set dPhi and dZ cuts - these are empirically defined so as to make
      // doubly counted hits disappear
      float dZcut = 20.; // mm
      float dPhicut = isBarrel ? 0.004 : std::numeric_limits<float>::max();

      // Otherwise check their overlap in dPhi
      TVector3 outHitPos(outhit->globalX(), outhit->globalY(), outhit->globalZ());
      float dPhi = std::abs(outHitPos.DeltaPhi(inHitPos));
      float dZ = std::abs(outHitPos.Z() - inHitPos.Z());
       if (dPhi < dPhicut && dZ < dZcut && deid(*inhit) != deid(*outhit)) {
  overlap = true;
  break;
      }
    }

    if (!overlap) {
      outHitsCollection.push_back(inhit);
    }

  }

  return outHitsCollection;

}
