#ifndef JOB_METADATA_HH
#define JOB_METADATA_HH

#include <string>
#include <cmath>
#include <vector>
#include <memory>
#include <chrono>
#include <filesystem>

#include "H5Cpp.h"
#include <nlohmann/json.hpp>

namespace xAOD {
  class EventInfo_v1;
  using EventInfo = EventInfo_v1;
}

typedef std::chrono::steady_clock Clock;
typedef std::chrono::milliseconds milliseconds;
typedef std::chrono::duration<float, milliseconds::period> float_ms;

class TimingStats {
  private:
    size_t m_n_jets;
    float m_mean; // ms
    float m_m2; // sum of squared difference from mean, ms^2

  public:
    std::string name;

    void update(float_ms time) {
      float time_ms = time.count();
      if (m_n_jets == 0) m_mean = time_ms;
      m_n_jets++;
      float delta = time_ms - m_mean;
      m_mean += delta / m_n_jets;
      float delta2 = time_ms - m_mean;
      m_m2 += delta * delta2;
    }

    std::tuple<size_t, float, float> get_stats() {
      float variance = m_m2 / m_n_jets;
      return {m_n_jets, m_mean, variance};
    }
};

void writeJobMetadata(
  const std::vector<std::shared_ptr<TimingStats>>&,
  const std::string& output_file = "userJobMetadata.json");

void addAttributeToHDF5(const std::filesystem::path& cfg_path, H5::Group& output_file);

#endif
